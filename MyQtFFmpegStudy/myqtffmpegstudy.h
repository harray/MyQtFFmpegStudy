#ifndef MYQTFFMPEGSTUDY_H
#define MYQTFFMPEGSTUDY_H

#include <QtWidgets/QWidget>
#include "ui_myqtffmpegstudy.h"

class MyQtFFmpegStudy : public QWidget
{
	Q_OBJECT

public:
	MyQtFFmpegStudy(QWidget *parent = 0);
	~MyQtFFmpegStudy();

private:
	Ui::MyQtFFmpegStudyClass ui;
};

#endif // MYQTFFMPEGSTUDY_H
